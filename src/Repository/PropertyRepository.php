<?php

namespace App\Repository;

use App\Entity\Property;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @method Property|null find($id, $lockMode = null, $lockVersion = null)
 * @method Property|null findOneBy(array $criteria, array $orderBy = null)
 * @method Property[]    findAll()
 * @method Property[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Property::class);
    }
    
//    public function findAllVisible() {
//        
//        return $this->createQueryBuilder('p') //this is an object that allows it to design a request, and give an alias;  
//                //when you want to access the table that contains the properties, need to use the p alias as a reference
//                ->andWhere('p.exampleField = :val')
//                ->setParameter('va', $value)
//                ->orderBy('p.id', 'ASC')
//                ->setMaxResults(10)
//                ->getQuery() //recoups the request
//                ->getResult(); //gets the results
//        
//    }
    /**
     * @return Property[]
     */
    
    public function findAllVisible(): array {
        
        return $this->findVisibleQuery()
                ->where('p.sold = false')
                ->getQuery()
                ->getResult();
    }
    
    /**
     * @return Property[] 
     */
    
    public function findLatest(): array {
        
        return $this->findVisibleQuery()
                ->where('p.sold = false')
                ->setMaxResults(4)
                ->getQuery()
                ->getResult();
    }
    
    private function findVisibleQuery(): QueryBuilder { //usually put private methods at the end
        //better to create this method and call it into other methods, this way if you want to change the search param,
        //only need to change them here and not in every method
         return $this->createQueryBuilder('p')
            ->where('p.sold = false');
    
    }

    // /**
    //  * @return Property[] Returns an array of Property objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Property
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}


