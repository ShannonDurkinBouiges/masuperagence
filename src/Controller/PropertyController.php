<?php

namespace App\Controller;

use App\Entity\Property;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Description of PropertyController
 *
 * @author didier
 */
class PropertyController extends AbstractController { //AbstractController injects directly the container, which contains the object that contains all the framework services
    
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PropertyRepository
     */
    private $repository;

    public function __construct(PropertyRepository $repository, EntityManagerInterface $em) {
        
        $this->repository = $repository;
        $this->em = $em;
    }

    
    /**
     * @Route("/biens", name="property.index")
     * @return Response
     */
    
    
    public function index(): Response {
       
        //$repository = $this->getDoctrine()->getRepository(Property::class);  can use this way or can do an injection
        //with command window 
        
        //$property = $this->repository->find(1);  //find() will find by id;
        //$property = $this->repository->findAll();   //will return an array with all objects;
        //$property = $this->repository->findOneBy(['floor' => 4]); //can specify a cetrain search
        
        //$property[0]->setSold(true);  //this is to change the first object to sold in the property
	return $this->render('property/index.html.twig',
                ['current_menu' => 'properties']);
    }
   
    
    //can write method like below or the second option
//    public function show($slug, $id): Response { //name of the param need to correspond with the name of the var above in @Route
//        $property = $this->repository->find($id);
//        return $this->render('property/show.html.twig', [
//            'property' => $property,
//            'current_menu' => 'properties'
//        ]);
//    }
    
     /**
     * @Route("/biens/{slug}.{id}", name="property.show", requirements={"slug": "[a-z0-9\-]*"})
     * @param Property $property
     * @return Response
     */
    public function show(Property $property, string $slug): Response {
    //public function show($slug, $id): Response {    
    //    $property = $this->repository->find($id);
        if ($property->getSlug() !== $slug){
            return $this->redirectToRoute('property.show',
                    [
                        'id' => $property->getId(),
                        'slug' => $property->getSlug()
                    ], 301);    // rediection permanente
        }
        return $this->render('property/show.html.twig',
           ['property' => $property,
            'current_menu' => 'properties'
           ]); 
 
    }
}



